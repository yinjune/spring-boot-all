package com.yjj.config.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: YinJunJie
 * @date: 2020/3/26 13:59
 * @description:
 */
@Component
@ConfigurationProperties(prefix = "test.config")
public class TestConfig {
    private String name;
    private Integer age;
    private List<String> hobbies;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    @Override
    public String toString() {
        return "TestConfig{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", hobbies=" + hobbies +
                '}';
    }
}
