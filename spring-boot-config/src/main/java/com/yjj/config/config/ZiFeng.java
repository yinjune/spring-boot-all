package com.yjj.config.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: YinJunJie
 * @date: 2020/3/26 14:52
 * @description:
 */
@Component
//PropertySource注解不支持读取yml文件 具体可见源码
@PropertySource(value = "classpath:zifeng.properties")
@ConfigurationProperties(prefix = "user")
public class ZiFeng {
    private String name;
    private Integer age;
    private List<String> desc;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setDesc(List<String> desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "ZiFeng{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", desc=" + desc +
                '}';
    }
}
