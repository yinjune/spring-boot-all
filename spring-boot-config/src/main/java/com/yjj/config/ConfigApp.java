package com.yjj.config;

import com.yjj.config.config.TestConfig;
import com.yjj.config.config.ZiFeng;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author: YinJunJie
 * @date: 2020/3/26 13:57
 * @description:
 */
@SpringBootApplication
public class ConfigApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext context=SpringApplication.run(ConfigApp.class, args);
        TestConfig config=context.getBean(TestConfig.class);
        System.out.println(config);
        ZiFeng ziFeng=context.getBean(ZiFeng.class);
        System.out.println(ziFeng);
    }
}
